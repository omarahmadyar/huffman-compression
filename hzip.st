#!/bin/gst -f
"#!/afs/cats.ucsc.edu/courses/cse112-wm/usr/smalltalk/bin/gst -f"

"Quality of Life"
nl := (Character nl).

"Character extension"
Character extend [
   isPrint [
      ^ (Character space <= self) & (self <= $~)
   ]
   visible [
      self isPrint ifTrue: [^ '$', self asString]
                   ifFalse: [^ self asInteger printStringRadix: 8]
   ]
]

" Frequency Table ---------------------------------------------------"
Object subclass: FreqTable [
  | table |

  " Constructor "
  FreqTable class >> new: size[
    | retVal |
    retVal := super new.
    retVal makeArray: size.
    ^retVal
  ]

  " Mutator "
  makeArray: size[
    table := Array new: size.
    1 to: size do: [:x |
      table at:x put:(Leaf new:(x-1) count:0).
    ].
  ]

  incValue: index [
    (table at:(index+1)) incCount
  ]

  " Accessors "
  table [
    ^table.
  ]
].
" End Frequency Table------------------------------------------------"

" Leaf Class --------------------------------------------------------"
Object subclass: Leaf [
  | key count |

  " Constructor "
  Leaf class >> new: key count:cnt[
    |retVal|
    retVal := super new.
    retVal setKey:key.
    retVal setCount:cnt.
    ^retVal
  ]

  " Mutator "
  incCount [
    count := (count + 1).
  ]
  setKey: ch [
    key := ch.
  ]

  setCount:cnt[
    count := cnt.
  ]

  " Accessors "
  key [
    ^key
  ]

  count [
    ^count
  ]

  " Other Accessors "
  printBase:stream [
    ^stream << self class << '(' << key asCharacter visible << ',' << count
  ]

  print:stream [
    ^stream << (self printBase: stream) << ')'.
  ]

  depthFirst:collection prefix: string [
    | arr |
    arr:= Array new:2.
    arr at:1 put:key.
    arr at:2 put:string.
    collection add: arr.
  ]

  isLeaf [
    ^true
  ]

  " Comparators "
  <= leafObj [
    (count = (leafObj count)) ifTrue:  [^(key <= (leafObj key))]
                              ifFalse: [^(count <= (leafObj count))].
  ]
].
" End Leaf Class ----------------------------------------------------"

" Tree Class --------------------------------------------------------"
Leaf subclass: Tree [
  | left right |

  " Constructor "
  Tree class >> new:lt right:rt [
    | retVal |
    retVal := super new:(lt key) count:((lt count)+(rt count)).
    retVal setLeft: lt.
    retVal setRight: rt.
    ^retVal.
  ]

  " Mutators "
  setLeft: lt [
    left := lt.
  ]

  setRight:rt [
    right := rt.
  ]

  " Accessors "
  left [
    ^left.
  ]

  right [
    ^right.
  ]

  isLeaf [
    ^false
  ]

  print: stream [
    (self printBase: stream) << ',' << (left key) << ',' << (right key).
  ]

  depthFirst: collection prefix: string[
    (self left ) depthFirst: (collection) prefix:(string, '0').
    (self right) depthFirst: (collection) prefix:(string, '1').
    collection add:(Array new: 1)
  ]
]
" End Tree Class ----------------------------------------------------"
" Bit Write Out Class -----------------------------------------------"
Object subclass: OutBits [
   |bitIndex currentByte myStream|
   OutBits class >> new [
      self shouldNotImplement.
   ]
   OutBits class >> new: fileStream [
      |result|
      result := super new.
      result init: fileStream.
      ^result
   ]
   clearByte [
      bitIndex := 8.
      currentByte := 0.
   ]
   init: fileStream [
      myStream := fileStream.
      self clearByte.
   ]
   flushByte [
      bitIndex = 8 ifFalse: [
         myStream nextPutByte: currentByte.
         self clearByte.
      ]
   ]
   writeBit: bit [
      currentByte := currentByte bitAt: bitIndex put: bit.
      bitIndex := bitIndex - 1.
      bitIndex = 0 ifTrue: [self flushByte].
   ]
]
" End Bit WO Class --------------------------------------------------"
" Bit Read Class ----------------------------------------------------"
Object subclass: BitStream [
   |bitIndex byte myStream|
   BitStream class >> new: fileStream [
      |result|
      result := super new.
      result init: fileStream.
      ^result
   ]
   init: fileStream [
      myStream := fileStream.
      bitIndex := 1.
   ]
   nextBit [
      bitIndex = 1 ifTrue: [
         byte := myStream next.
         bitIndex := 9.
      ].
      bitIndex := bitIndex - 1.
      ^byte value bitAt: bitIndex
   ]
   atEnd [
      ^bitIndex = 1 and: [myStream atEnd]
   ]
   currByte [
      ^byte
   ]
]
" End Bit Read Class ------------------------------------------------"

"===================================================================="
"=========================== M A I N ================================"
"===================================================================="
"Reading arguments"
MODE := 0. "1=c, 2=u, 3=t, 4=d"
(Smalltalk arguments at: 1) = '-c' ifTrue: [MODE := 1].
(Smalltalk arguments at: 1) = '-u' ifTrue: [MODE := 2].
(Smalltalk arguments at: 1) = '-t' ifTrue: [MODE := 3].
(Smalltalk arguments at: 1) = '-d' ifTrue: [MODE := 4].
(MODE=0) ifTrue: [
  stderr << 'Error: no valid mode chosen.' << nl.
  ObjectMemory quit:1.
].

(Smalltalk arguments size) < 2 ifTrue: [
  stderr << 'Error: not enough arguments.' << nl.
  ObjectMemory quit:2.
].

" In/Out Files "
infile := 0.
infile := (Smalltalk arguments at: 2).
infile := FileStream open:infile mode:(FileStream read).

outfile := stdout.
(Smalltalk arguments size) >= 3 ifTrue: [
  outfile := (Smalltalk arguments at:3).
].
(outfile = stdout) ifFalse: [outfile := FileStream open:outfile mode:(FileStream write).].

" Compression *********************************************"
((MODE=1) or: [MODE=3.]) ifTrue: [
    | Codes Frequencies table mapping bitstream |
  " Make Freq Table "
  Frequencies := FreqTable new:257.
  [infile atEnd not] whileTrue: [
    | nextnum |
    nextnum := (infile next asInteger).
    Frequencies incValue: nextnum.
  ].
  Frequencies incValue: 256.

  " Sort and Remove 0 Freq elements "
  table := (Frequencies table) asSortedCollection.
  [(table at: 1) count = 0] whileTrue: [
    table removeFirst.
  ].

  " Create Codings "
  [(table size) > 1] whileTrue: [
    | lft rgt |
    lft := table at: 1.
    table removeFirst.
    rgt := table at: 1.
    table removeFirst.
    tree := (Tree new:lft right:rgt).
    table add: tree.
  ].

  " Get mappings for ascii values to codes in the form of a Collection of Arrays"
  Codes := OrderedCollection new.
  (table at:1 ) depthFirst: Codes prefix:''.

  " Print out encoding table if -t was passed as a flag "
  (MODE=3)ifTrue: [
    1 to: (Codes size) do: [:x |
      (((Codes at:x) size) = 1) ifFalse: [
        | key val |
        key := (Codes at:x) at:1.
        val := (Codes at:x) at:2.
        (key > 32 and: [key < 127])
          ifTrue: [stdout << ' ' << key asCharacter << ' '.]
          ifFalse:[(key=256)ifTrue:[stdout << 'EOF']
                            ifFalse:[stdout << (key printPaddedWith:(Character space) to:3) .]].
        stdout << '     ' << ((((Frequencies table) at:(key+1)) count) printPaddedWith:(Character space) to:3) << ' ' << val << nl
      ].
    ].
  ].

  " Construct Dictionary "
  mapping := Dictionary new.
  1 to: (Codes size) do: [:x |
    (((Codes at:x) size) = 1) ifFalse: [
      mapping at: ((Codes at: x)at:1) put:((Codes at:x)at:2).
    ].
  ].

  " Write tree structure to outfile "
  bitstream := OutBits new: outfile.
  1 to: (Codes size) do: [:x |
    (((Codes at:x)size)=1)
      ifFalse: [
        | ascii |
        ascii := (Codes at:x)at:1.
        bitstream writeBit:0.
        8 to:1 by:-1 do: [:y |
          bitstream writeBit:(ascii bitAt:y).
        ].
        (ascii=256) ifTrue: [bitstream writeBit:1]. "special case for eof"
        (ascii=0  ) ifTrue: [bitstream writeBit:0]. "special case for null"
      ]
      ifTrue: [
        bitstream writeBit:1.
      ].

  ].
  bitstream writeBit:1. "terminating single bit"

  " Write coded version of infile to outfile "
  infile position: 0.
  [infile atEnd not] whileTrue: [
    | code |
    code := mapping at:(infile next asInteger).
    1 to:(code size) do: [ :i |
      bitstream writeBit:(((code at:i)asInteger) - ($0 asInteger)).
    ].
  ].
  "Write eof code"
  code := mapping at:256.
  1 to:(code size) do: [ :i |
    bitstream writeBit:(((code at:i)asInteger) - ($0 asInteger)).
  ].
  bitstream flushByte.
].

" Uncompress **********************************************"
(MODE=2) ifTrue: [
  | bitstream stack treeHead cond node |
  bitstream := BitStream new: infile.

  " Generate Tree - Read in Huffman Tree for ACII values "
  stack := OrderedCollection new.
  cond := true.
  [cond] whileTrue: [
    ((bitstream nextBit)=1)
      ifTrue: [
        ((stack size)=1)
          ifTrue:  [ cond:= false. ]
          ifFalse: [
            | left right tree |
            right := stack first.
            stack removeFirst.
            left := stack first.
            stack removeFirst.
            tree := Tree new:left right:right.
            stack addFirst: tree.
          ].
      ]
      ifFalse:[
        | byte leaf |
        byte := 0.
        8 to: 1 by: -1 do: [:index | byte:= byte bitAt: index put:(bitstream nextBit)].
        (byte=0) ifTrue: [ ((bitstream nextBit)=1) ifTrue: [byte:=256] ].
        leaf := Leaf new:byte count:0.
        stack addFirst: leaf.
      ].
  ].
  treeHead := stack at:1.

  " Write decoded version of infile to outfile "
  node := treeHead.
  cond := true.
  [cond] whileTrue: [
    ((bitstream nextBit)=1) ifTrue:  [ node := node right ]
                            ifFalse: [ node := node left ].
    (node isLeaf) ifTrue: [
      ((node key) = 256)  ifTrue:   [ cond := false ]
                          ifFalse:  [ outfile nextPutByte:(node key) ].
      node := treeHead
    ]
  ]
].

" Finish Up "
infile close.
outfile close.

